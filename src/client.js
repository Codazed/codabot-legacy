const chalk = require("chalk");
const logger = require("loglevel");
const prefix = require("loglevel-plugin-prefix");
const config = require("../config/config.json");
const Bot = require("./bot");
var bot;

// Chalk colors
const colors = {
    DEBUG: chalk.cyan,
    INFO: chalk.blue,
    WARN: chalk.yellow,
    ERROR: chalk.red,
    twitch: chalk.hex('#9146FF'),
    discord: chalk.hex('#7289DA')
}

prefix.reg(logger);
logger.enableAll();
prefix.apply(logger, {
    format(level, name, timestamp) {
        return `${chalk.gray(`[${timestamp}]`)} ${colors[level.toUpperCase()](level + ":")}`;
    },
});

class Client {

    constructor() {
        bot = new Bot(this);
    }

    log(message, loglevel = 'info', module='none') {
        if (loglevel.toLowerCase() === 'debug' && (config.devMode || config.loglevel === 0)) {
            logger.debug(message);
        } else {
            switch (module.toLowerCase()) {
                case 'twitch':
                    message = `${colors.twitch("[Twitch Module]")} ${message}`;
                    break;
                case 'discord':
                    message = `${colors.discord("[Discord Module]")} ${message}`;
                    break;
            }
            switch (loglevel.toLowerCase()) {
                case 'info':
                    logger.info(message);
                    break;
                case 'warn':
                    logger.warn(message);
                    break;
                case 'err':
                    logger.error(message);
                    break;
            }
        }
    }

    runBot() {
        bot.start()
    }

    exit() {
        process.exit(0);
    }
}

module.exports = Client;