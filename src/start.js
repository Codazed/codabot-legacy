const fs = require("fs");
const prompts = require("prompts");

if (!fs.existsSync("./config/config.json")) {
    fs.copyFile("./config/config-default.json", "./config/config.json", (err) => {
        if (err) throw err;
    });
}

if (!fs.existsSync("./config/apis.json")) {
    runSetup();
} else {
    const Client = require("./client");
    const client = new Client();
    client.runBot();
}

if (!fs.existsSync("./data")) {
    fs.mkdirSync("./data");
}

if (!fs.existsSync("./cache")) {
    fs.mkdirSync("./cache");
}

function runSetup() {
    console.log("Performing first-time setup...");
    let apis = {
        discord: undefined,
        youtube: undefined,
        twitch: undefined
    }
    const questions = [
        {
            type: 'text',
            name: 'discord',
            message: 'What is my Discord bot token?'
        },
        {
            type: 'text',
            name: 'youtube',
            message: 'What is my YouTube token? (Leave blank to disable YouTube features)'
        },
        {
            type: 'text',
            name: 'twitch',
            message: 'What is my Twitch Oauth token? (Leave blank to disable Twitch features)'
        },
        {
            type: 'text',
            name: 'twitchname',
            message: 'What is my Twitch bot username? (Leave blank if not using Twitch features)'
        }
    ];

    (async () => {
        const response = await prompts(questions);
        apis.discord = response.discord;
        if (response.youtube !== "") { apis.youtube = response.youtube; }
        if (response.twitch !== "") { apis.twitch = {
            'username': response.twitchname,
            'oauth': response.twitch
        };}
        fs.writeFile("./config/apis.json", JSON.stringify(apis), function(error) {
            console.log("First-time setup completed! Bot now ready to be used.");
        const Client = require("./client");
        const client = new Client();
        client.runBot();
        });
    })();

    /* prompt("What is my bot token?\n", function (input) {
        apis.discord = input;
        prompt("What is my YouTube token? (Leave blank to disable YouTube features)\n", function (input) {
            if (input !== "") {
                apis.youtube = input;
            }
            prompt("What is my Twitch Oauth token? (Leave blank to disable Twitch features)\n", function (input) {
                if (input !== "") {
                    apis.twitch = input;
                    fs.writeFileSync("./config/apis.json", JSON.stringify(apis));
                    console.log("First-time setup completed! Bot now ready to be used.");
                    const Client = require("./client");
                    const client = new Client();
                    client.runBot();
                }
            });
        });
    }); */
}

function prompt(question, callback) {
    var stdin = process.stdin,
        stdout = process.stdout;

    stdin.resume();
    stdout.write(question);

    stdin.once('data', function (data) {
        callback(data.toString().trim());
    });
}