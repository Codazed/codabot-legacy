var config = require("../config/config.json");
const apis = require("../config/apis.json");
const fs = require("fs");
const UrlPattern = require('url-pattern');
const chalk = require("chalk");
const ffmpeg = require("ffmpeg");
const youtubedl = require("youtube-dl");
const ytsearch = require("youtube-search");
const YouTube = require("simple-youtube-api");
const Discord = require("discord.js");
const bot = new Discord.Client();
const TwitchBot = require("twitch-bot");
var twBot;
var log;
var client;

let ytsearchopts = {
    maxResults: 1,
    key: apis.youtube
}

let commands = {
    "help": config.cmdprefix + "help",
    "twitch": config.cmdprefix + "twitch",
    "playlist": config.cmdprefix + "playlist",
    "play": config.cmdprefix + "play",
    "stop": config.cmdprefix + "stop",
    "loop": config.cmdprefix + "loop",
    "skip": config.cmdprefix + "skip",
    "shutdown": config.cmdprefix + "shutdown",
    "restart": config.cmdprefix + "restart",
    "resetstatus": config.cmdprefix + "resetstatus",
    "dumpqueue": config.cmdprefix + "dumpqueue"
}

let loadingmsg = [];
let stopRequest = false;
let dispatcher;
let audioPlaying = false;
let statuses = [];
let playlistStatus = {
    "downloading": false,
    "remaining": 0
}
let vidQueue = [];
let loopEnabled = false;

class Bot {
    constructor(nodeclient) {
        log = nodeclient.log;
        client = nodeclient;
    }

    start() {
        bot.login(apis.discord);
    }
}

let twitchbots = [];

bot.on('ready', () => {
    let guilds = bot.guilds.keyArray();
    guilds.forEach(function (item, index) {
        if (!fs.existsSync('./data/' + item)) {
            fs.mkdirSync('./data/' + item);
            log('Created data folder for guild ' + item, 'debug');
        }
        if (!fs.existsSync('./cache/' + item)) {
            fs.mkdirSync('./cache/' + item);
            log('Created cache folder for guild ' + item, 'debug');
        }
        if (!fs.existsSync(`./data/${item}/twitch.json`)) {
            var defaults = {
                "channels": [],
                "clipchannel": null
            }
            fs.writeFile(`./data/${item}/twitch.json`, JSON.stringify(defaults), function (error) {
                if (error) log(error, "ERR");
            });
        } else {
            if (apis.twitch !== undefined) {
                let svrTwitchCfg = require(`../data/${item}/twitch.json`);
                if (svrTwitchCfg.channels.length > 0) {
                    twBot = new TwitchBot({
                        username: apis.twitch.username,
                        oauth: apis.twitch.oauth,
                        channels: svrTwitchCfg.channels
                    });
                    twitchbots[item] = twBot;
                    log(`Created Twitch bot for server id ${item}`);
                    setTwitchListeners(item);
                } else {
                    log(`Invalid Twitch configuration for server ${chalk.green(item)}. Some configuration info may be missing. Skipping bot creation for this server.`, 'warn', 'twitch');
                }
            }
        }

    });
    fs.readFile('./config/statuses.txt', function (err, data) {
        if (err) throw err;
        statuses = data.toString().split("\n");
        resetSelf();
    });
    log('Logged in', 'info', 'discord');
    if (config.devMode) {
        log('Bot running in development mode!', 'warn');
        bot.user.setStatus('dnd');
    }
    if (!fs.existsSync('./data')) {
        fs.mkdirSync('./data');
    }
    if (!fs.existsSync('./cache')) {
        fs.mkdirSync('./cache');
    }
});

bot.on('message', msg => {

    let datadir = "./data/" + msg.guild.id + "/";
    let cachedir = "./cache/" + msg.guild.id + "/";

    if (msg.member === null) {
        return;
    }

    let voiceChnl = msg.member.voiceChannel;

    if (msg.content.startsWith(config.cmdprefix)) {
        log(`${chalk.gray(`(${msg.guild.id})`)} Command ${chalk.bgCyan(chalk.black(msg.content))} executed by user ${chalk.bgGreen(chalk.black(`${msg.member.user.tag} (${msg.member.user.id})`))}`, 'debug');
    }

    // Help information
    if (msg.content.startsWith(commands.help)) {
        msg.channel.send(`You can view my command reference here: https://gitlab.com/Codazed/coda/wikis/Command-Reference`);
    }

    // Twitch module controller
    else if (msg.content.startsWith(commands.twitch)) {
        // Retrieve Twitch config for current server
        let tcfg = require(`../data/${msg.guild.id}/twitch.json`);

        let args = msg.content.split(" ");

        // Select Twitch clips channel
        if (args[1] === "here") {
            let channel = msg.channel;
            let updateConfig = tcfg;
            updateConfig.clipchannel = channel;
            fs.writeFile(`./data/${msg.channel.guild.id}/twitch.json`, JSON.stringify(updateConfig, "", " "), function (err) {
                if (err) throw err;
                msg.channel.send("Clips for monitored Twitch channels will now be posted in this channel.");
                log(`Twitch clips for ${chalk.green(msg.channel.guild.name)} will be posted in ${chalk.blue(`#${msg.channel.name}`)}`);
                reloadTwitch(msg.guild.id);
            });
        }

        // Add Twitch channels to clip monitoring list
        else if (args[1] === "add") {
            args.shift();
            args.shift();
            let twitchchannel = args;
            let updateConfig = tcfg;
            args.forEach(function (item, index) {
                updateConfig.channels.push(item);
            });
            fs.writeFile(`./data/${msg.channel.guild.id}/twitch.json`, JSON.stringify(updateConfig, "", " "), function (err) {
                if (err) throw err;
                msg.channel.send(`Added ${twitchchannel} to list of monitored Twitch channels.`);
                log(`Added ${chalk.green(twitchchannel)} to list of Twitch channels for server ${chalk.blue(`${msg.channel.guild.name} - (${msg.channel.guild.id})`)}`);
                reloadTwitch(msg.guild.id);
            });
        }

        // Remove Twitch channels from clip monitoring list
        else if (args[1] === "remove") {
            args.shift();
            args.shift();
            let twitchchannel = args;
            let updateConfig = tcfg;
            args.forEach(function (item, index) {
                updateConfig.channels.splice(updateConfig.channels.indexOf(item), 1);
            });
            fs.writeFile(`./data/${msg.channel.guild.id}/twitch.json`, JSON.stringify(updateConfig, "", " "), function (err) {
                if (err) throw err;
                msg.channel.send(`Removed ${twitchchannel} from list of monitored Twitch channels.`);
                log(`Removed ${chalk.green(twitchchannel)} from list of Twitch channels for server ${chalk.blue(`${msg.channel.guild.name} - (${msg.channel.guild.id})`)}`);
                reloadTwitch(msg.guild.id);
            });
        }
    }

    // Playlist handling
    else if (msg.content.startsWith(commands.playlist)) {
        if (!fs.existsSync(datadir + "playlists")) {
            fs.mkdirSync(datadir + "playlists");
        }
        let args = msg.content.split(" ");

        // Create new playlist
        if (args[1] === "create") {
            let playlistName = args[2];
            if (fs.existsSync(datadir + "playlists/" + playlistName + '.json')) {
                msg.channel.send("Error: Playlist `" + playlistName + "` already exists.");
            } else {
                let playlistinfo = {
                    "name": playlistName,
                    "creator": msg.author.id,
                    "size": 0,
                    "videos": []
                }
                fs.writeFile(datadir + "playlists/" + playlistName + ".json", JSON.stringify(playlistinfo, "", " "), (err) => {
                    if (err) throw err;
                    log(`${chalk.gray(`(${msg.guild.id})`)} Playlist ${chalk.cyan(playlistName)} created.`);
                    msg.channel.send("Playlist `" + playlistName + "` successfully created. Add things to it with " + config.cmdprefix + "playlist add " + playlistName + " `video url`");
                });
            }
        }

        // Delete playlist
        else if (args[1] === "delete") {
            let playlistName = args[2];
            if (!fs.existsSync(datadir + "playlists/" + playlistName + '.json')) {
                msg.channel.send("Error: Playlist `" + playlistName + "` does not exist.");
            } else {
                fs.unlink(datadir + "playlists/" + playlistName + '.json', (err) => {
                    if (err) throw err;
                    log(`${chalk.gray(`(${msg.guild.id})`)} Deleted playlist ${chalk.cyan(playlistName)}.`);
                    msg.channel.send("Playlist `" + playlistName + "` was successfully deleted.");
                })
            }
        }

        // Add items to a playlist
        else if (args[1] === "add") {
            let playlistName = args[2];
            if (!fs.existsSync(datadir + "playlists/" + playlistName + '.json')) {
                msg.channel.send("Error: Playlist `" + playlistName + "` does not exist.");
            } else {
                let videoUrl = args[3];
                if (videoUrl.startsWith("https://youtube.com/watch?v=") || videoUrl.startsWith("https://www.youtube.com/watch?v=") || videoUrl.startsWith("https://youtu.be/")) {
                    fs.readFile(datadir + "playlists/" + playlistName + ".json", 'utf8', function (err, data) {
                        if (err) throw err;
                        let requestedPlaylist = JSON.parse(data);
                        let videos = requestedPlaylist.videos;
                        let size = requestedPlaylist.size;
                        size++;
                        videos.push(videoUrl);
                        requestedPlaylist.videos = videos;
                        requestedPlaylist.size = size;
                        fs.writeFile(datadir + "playlists/" + playlistName + '.json', JSON.stringify(requestedPlaylist, "", " "), (err) => {
                            if (err) throw err;
                            log(`${chalk.bgGreen(chalk.black(msg.author.tag))} added ${chalk.yellow(videoUrl)} to the ${chalk.red(playlistName)} playlist.`);
                            msg.channel.send(msg.author.username + " added " + videoUrl + " to the `" + playlistName + "` playlist.");
                        });
                    });
                } else {
                    msg.channel.send("Error: Only YouTube links are supported.");
                }
            }
        }

        // Remove items from a playlist
        else if (args[1] === "remove") {
            let playlistName = args[2];
            if (!fs.existsSync(datadir + "playlists/" + playlistName + '.json')) {
                msg.channel.send("Error: Playlist `" + playlistName + "` does not exist.");
            } else {
                let videoUrl = args[3];
                let getYtId = require('get-youtube-id');
                let requestedId = getYtId(videoUrl);
                if (videoUrl.startsWith("https://youtube.com/watch?v=") || videoUrl.startsWith("https://www.youtube.com/watch?v=") || videoUrl.startsWith("https://youtu.be/")) {
                    fs.readFile(datadir + "playlists/" + playlistName + ".json", 'utf8', function (err, data) {
                        if (err) throw err;
                        let requestedPlaylist = JSON.parse(data);
                        let videos = requestedPlaylist.videos;
                        let newvids = []
                        videos.forEach(element => {
                            if (getYtId(element) !== requestedId) {
                                newvids.push(element);
                            }
                        });
                        requestedPlaylist.videos = newvids;
                        let newsize = newvids.length;
                        requestedPlaylist.size = newsize;
                        fs.writeFile(datadir + "playlists/" + playlistName + '.json', JSON.stringify(requestedPlaylist, "", " "), (err) => {
                            if (err) throw err;
                            log(`${chalk.gray(`(${msg.guild.id})`)} ${chalk.bgGreen(chalk.black(msg.author.tag))} removed ${chalk.yellow(videoUrl)} from the ${chalk.red(playlistName)} playlist.`);
                            msg.channel.send(msg.author.username + " removed " + videoUrl + " from the `" + playlistName + "` playlist.");
                        });
                    });
                } else {
                    msg.channel.send("Error: Only YouTube links are supported.");
                }
            }
        }


        // Import playlist from YouTube
        else if (args[1] === "import") {
            let playlistUrl = args[2];
            let playlistName = args[3];
            if (fs.existsSync(datadir + "playlists/" + playlistName + '.json')) {
                msg.channel.send("Error: Playlist `" + playlistName + "` already exists.");
            } else {
                let youtube = new YouTube(apis.youtube);
                youtube.getPlaylist(playlistUrl).then(playlist => {
                    log(`${chalk.gray(`(${msg.guild.id})`)} Importing playlist...`);
                    notifyProcessing(msg.channel, "Importing playlist...");
                    playlist.getVideos().then(videos => {
                        let links = [];
                        videos.forEach(element => {
                            let link = "https://youtu.be/" + element.channel.raw.snippet.resourceId.videoId;
                            links.push(link);
                        });
                        let playlistinfo = {
                            "name": playlistName,
                            "creator": msg.author.id,
                            "size": links.length,
                            "videos": links
                        }
                        fs.writeFile(datadir + "playlists/" + playlistName + '.json', JSON.stringify(playlistinfo, "", " "), (err) => {
                            if (err) throw err;
                            log(`${chalk.gray(`(${msg.guild.id})`)} Playlist imported`);
                            stopNotifyProcessing();
                            let embed = new Discord.RichEmbed();
                            embed.setColor("#FF0000");
                            embed.setTitle("Playlist successfully imported!");
                            embed.addField("Name", playlistinfo.name, true);
                            embed.addField("Size", playlistinfo.size, true);
                            msg.channel.send(embed);
                        });
                    }).catch(err => {
                        log(err, 'err');
                    });
                });
            }
        }

        // Play playlist in order
        else if (args[1] === "play") {
            stopRequest = false;
            if (args[2] != undefined) {
                notifyProcessing(msg.channel);
                fs.readFile(datadir + "playlists/" + args[2] + ".json", 'utf8', function (err, data) {
                    if (err) throw err;
                    let requestedPlaylist = JSON.parse(data);
                    queuePlaylist(requestedPlaylist.videos, msg);
                });
            }

        }

        // Play playlist in random order
        else if (args[1] === "mix") {
            stopRequest = false;
            if (args[2] != undefined) {
                notifyProcessing(msg.channel);
                fs.readFile(datadir + "playlists/" + args[2] + ".json", 'utf8', function (err, data) {
                    if (err) throw err;
                    let requestedPlaylist = JSON.parse(data);
                    let shuffle = require('shuffle-array');
                    let processedArray = requestedPlaylist.videos;
                    shuffle(processedArray);
                    queuePlaylist(processedArray, msg);
                });
            }
        }

        // List playlists
        else if (args[1] === "list") {
            fs.readdir(datadir + "playlists", function (err, files) {
                if (err) log(err, 'err');
                let playlists = [];
                files.forEach(function (file) {
                    let name = file.replace('.json', '');
                    playlists.push(name);
                });
                if (playlists.length > 0) {
                    msg.channel.send("Playlists on this server:\n" + playlists);
                } else {
                    msg.channel.send("There are no playlists on this server. Create or import one.");
                }
            })
        }

    }

    // Play music
    else if (msg.content.startsWith(commands.play)) {
        stopRequest = false;
        if (playlistStatus.downloading) {
            msg.channel.send("A playlist is currently downloading in the queue, please do /stop if you want to play something else at this time, or wait for the current playlist to be completely added to the queue. Videos remaining to be downloaded: " + playlistStatus.remaining);
        } else if (msg.content.length <= commands.play.length) {
            if (msg.attachments.array().length > 0) {
                if (msg.attachments.first().filename.endsWith(".mp3") || msg.attachments.first().filename.endsWith(".wav") || msg.attachments.first().filename.endsWith(".flac") || msg.attachments.first().filename.endsWith(".ogg")) {
                    queueVideo(msg.attachments.first().url, msg, true, msg.attachments.first());
                } else {
                    msg.channel.send("I can only play mp3, wav, flac, and ogg files right now. Convert your file to one of those types and try again.");
                }
            }
        } else {
            notifyProcessing(msg.channel);
            let ytquery = msg.content.substring(commands.play.length + 1);
            let youtubeRegex = require('youtube-regex');
            if (youtubeRegex().test(ytquery)) {
                queueVideo(ytquery, msg, false);
            } else {
                // Search YouTube
                searchYoutube(ytquery, msg, function (link) {
                    queueVideo(link, msg, false);
                });
            }
        }
    }

    // Stop playing music
    else if (msg.content.startsWith(commands.stop)) {
        if (audioPlaying) {
            vidQueue = []; // Clear queue
            stopRequest = true; // Signal a stop request to currently running functions
            dispatcher.end("Requested to stop.");
            resetSelf(msg.guild);
        }
    }

    // Toggle loop on/off
    else if (msg.content.startsWith(commands.loop)) {
        if (audioPlaying) {
            if (loopEnabled) {
                msg.channel.send("Loop disabled.");
                loopEnabled = false;
            } else {
                msg.channel.send("Loop enabled for the current song.");
                loopEnabled = true;
            }
        } else {
            msg.channel.send("There is no audio playing!");
        }
    }

    // Skip current video
    else if (msg.content.startsWith(commands.skip)) {
        if (audioPlaying) {
            dispatcher.end("Skipping...");
        } else {
            msg.channel.send("There is no audio playing!");
        }
    }

    // Shutdown bot (requires Manage Server permission)
    else if (msg.content.startsWith(commands.shutdown)) {
        if (msg.member.hasPermission('MANAGE_GUILD')) {
            log("Shutdown request received.");
            resetSelf(msg.guild);
            msg.channel.send("Goodbye! Miss you already! :heart:");
            setTimeout(function () {
                bot.destroy();
                setTimeout(function () {
                    client.exit();
                }, 1500);
            }, 2000);
        } else {
            msg.channel.send("This command may only be run by those with the 'Manage Server' permission.");
        }
    }

    // Restart bot (requires Manage Server permission)
    else if (msg.content.startsWith(commands.restart)) {
        if (msg.member.hasPermission('MANAGE_GUILD')) {
            log("Restart request received.");
            msg.channel.send("Restarting...");
            bot.destroy().then(status => {
                setTimeout(function () {
                    bot.login(apis.discord);
                }, 3000);
            });
        } else {
            msg.channel.send("This command may only be run by those with the 'Manage Server' permission.");
        }
    }

    // Reset status (for debug purposes)
    else if (msg.content.startsWith(commands.resetstatus)) {
        resetSelf();
    }

    // Dump song queue to file (for debug purposes)
    else if (msg.content.startsWith(commands.dumpqueue)) {
        fs.writeFile(datadir + "queuedump.json", JSON.stringify(vidQueue, "", " "), function (err) {
            if (err) throw err;
        });
    }

    // Delete messages containing commands
    if (msg.content.startsWith(config.cmdprefix) && msg.attachments.array().length == 0) {
        msg.delete().then(msg => {}).catch(console.error);
    }
});

function searchYoutube(query, msg, cb) {
    log(`${chalk.gray(`(${msg.guild.id})`)} YouTube search query was ${chalk.yellow(query)}`);
    ytsearch(query, ytsearchopts, function (err, results) {
        if (err) return log(err, 'err');
        cb(results[0].link);
    })
}

function playVideo(videoData) {
    let voiceChnl = videoData.voiceChannel;
    if (voiceChnl != undefined) {
        voiceChnl.join().then(connection => {
            audioPlaying = true;
            sendNowPlayingEmbed(videoData);
            decodeBase64AudioStream(videoData.audiostream, videoData.msg, function (decodedData) {
                dispatcher = connection.playStream(decodedData, {
                    volume: config.audioVolume
                });
            });
            bot.user.setActivity(videoData.title);
            dispatcher.on("end", () => {
                if (loopEnabled) {
                    playVideo(videoData);
                } else {
                    if (queueEmpty()) {
                        videoData.responseChannel.send("Queue is empty. Disconnecting.");
                        voiceChnl.leave();
                        resetSelf()
                        audioPlaying = false;
                    } else {
                        let nextVideo = vidQueue.shift();
                        playVideo(nextVideo);
                    }
                }
            })
        }).catch(console.error);
    }
}

function queueEmpty() {
    return vidQueue.length <= 0;
}

function queuePlaylist(linksarray, msg, echofull = true) {
    let cachedir = "./cache/" + msg.guild.id + "/";
    // If there are no more videos to queue
    if (linksarray.length == 0 && !stopRequest) {
        playlistStatus.downloading = false;
        playlistStatus.remaining = 0;
        log(`${chalk.gray(`(${msg.guild.id})`)} All playlist videos have been loaded.`);
    } else if (stopRequest) {
        linksarray = [];
        playlistStatus.downloading = false;
        playlistStatus.remaining = 0;
    }

    // If the queue is full, wait 15 seconds to see if a slot frees up, then try again
    else if (vidQueue.length >= config.maxQueueSize) {
        if (echofull) log(`${chalk.gray(`(${msg.guild.id})`)} Queue is full, checking every 5 seconds for a free slot.`);
        setTimeout(queuePlaylist, 5000, linksarray, msg, false);
    }

    // Queue a playlist video
    else {
        playlistStatus.downloading = true;
        playlistStatus.remaining = linksarray.length;
        log(`${chalk.gray(`(${msg.guild.id})`)} Downloading next playlist video... (${linksarray.length} videos left)`);
        let currentvid = linksarray.shift();
        youtubedl.getInfo(currentvid, function (err, data) {
            let video = youtubedl(currentvid);
            video.pipe(fs.createWriteStream(`${cachedir}/${data.id}`));
            video.on('end', function () {
                log(`${chalk.gray(`(${msg.guild.id})`)} Video download complete!`);
                encodeBase64AudioStream(`${cachedir}/${data.id}`, msg, function (encodedData) {
                    let queuer = "";
                    if (msg.member.nickname != null) {
                        queuer = msg.member.nickname;
                    } else {
                        queuer = msg.author.username;
                    }
                    let videoData = {
                        "source": "YouTube Playlist",
                        "audiostream": encodedData,
                        "title": data.title,
                        "uploader": data.uploader,
                        "length": data._duration_hms,
                        "thumbnail": data.thumbnail,
                        "url": data.webpage_url,
                        "queuer": queuer,
                        "msg": msg,
                        "queuerAvatar": msg.member.user.avatarURL,
                        "responseChannel": msg.channel,
                        "voiceChannel": msg.member.voiceChannel
                    }
                    stopNotifyProcessing();
                    if (stopRequest) {
                        // Do nothing
                    } else if (!audioPlaying) {
                        playVideo(videoData);
                    } else {
                        vidQueue.push(videoData);
                    }
                    queuePlaylist(linksarray, msg);
                });
            });
        });
    }
}

function queueVideo(link, msg, local, attachmentInfo = null, sendEmbed = true) {
    let cachedir = "./cache/" + msg.guild.id + "/";
    if (!local) {
        youtubedl.getInfo(link, function (err, data) {
            let queuer = "";
            if (msg.member.nickname != null) {
                queuer = msg.member.nickname;
            } else {
                queuer = msg.author.username;
            }
            let videoData = {
                "source": "YouTube",
                "audiostream": undefined,
                "title": data.title,
                "uploader": data.uploader,
                "length": data._duration_hms,
                "thumbnail": data.thumbnail,
                "url": data.webpage_url,
                "queuer": queuer,
                "msg": msg,
                "queuerAvatar": msg.member.user.avatarURL,
                "responseChannel": msg.channel,
                "voiceChannel": msg.member.voiceChannel
            }
            if (fs.existsSync(`${cachedir}/${data.id}.mp3`)) {
                log(`Video already exists in cache! Not downloading again.`);
                encodeBase64AudioStream(`${cachedir}/${data.id}.mp3`, msg, function (encodedData) {
                    videoData.audiostream = encodedData;
                    stopNotifyProcessing();
                    if (!audioPlaying) {
                        playVideo(videoData, msg);
                    } else {
                        vidQueue.push(videoData);
                        if (sendEmbed) {
                            sendQueueEmbed(videoData);
                        }
                    }
                });
            } else {
                let video = youtubedl(link);
                video.pipe(fs.createWriteStream(`${cachedir}/${data.id}`));
                video.on('end', function () {
                    log(`${chalk.gray(`(${msg.guild.id})`)} Video download complete!`);
                    if (!audioPlaying) {
                        encodeBase64AudioStream(`${cachedir}/${data.id}`, msg, function (encodedData) {
                            videoData.audiostream = encodedData;
                            stopNotifyProcessing();
                            playVideo(videoData, msg);
                            if (config.audiocache) {
                                log(`Audio cache is enabled. Converting to mp3.`, 'debug');
                                convertToAudio(`${cachedir}/${data.id}`);
                            } else {
                                log(`Audio cache is disabled. Deleting downloaded file.`, 'debug');
                                fs.unlinkSync(`${cachedir}/${data.id}`);
                            }
                        });
                    } else {
                        log(`Audio is currently playing. Converting to mp3 to save queue ram.`, 'debug');
                        convertToAudio(`${cachedir}/${data.id}`, true, msg, function (base64Data) {
                            videoData.audiostream = base64Data;
                            vidQueue.push(videoData);
                            if (sendEmbed) {
                                sendQueueEmbed(videoData);
                            }
                            if (!config.audiocache) {
                                log(`Audio cache is disabled. Deleting converted file.`, 'debug');
                                fs.unlinkSync(`${cachedir}/${data.id}.mp3`);
                            }
                        });
                    }
                });
            }
        });
    } else {
        log(`${chalk.gray(`(${msg.guild.id})`)} Downloading audio attachment...`);
        let download = request(link).pipe(fs.createWriteStream(`${cachedir}/${data.id}`));
        download.on('finish', function () {
            let musicDuration = require('music-duration');
            let gethhmmss = require('gethhmmss');
            let length;
            musicDuration(`${cachedir}/${data.id}`).then(duration => {
                length = gethhmmss(parseInt(duration));
                msg.delete();
                encodeBase64AudioStream(`${cachedir}/${data.id}`, function (encodedData) {
                    let queuer = "";
                    if (msg.member.nickname != null) {
                        queuer = msg.member.nickname;
                    } else {
                        queuer = msg.author.username;
                    }
                    let videoData = {
                        "source": "Uploaded Audio",
                        "audiostream": encodedData,
                        "title": attachmentInfo.filename,
                        "uploader": queuer,
                        "length": length,
                        "thumbnail": undefined,
                        "url": link,
                        "queuer": queuer,
                        "msg": msg,
                        "queuerAvatar": msg.member.user.avatarURL,
                        "responseChannel": msg.channel,
                        "voiceChannel": msg.member.voiceChannel
                    }
                    if (!audioPlaying) {
                        playVideo(videoData, msg);
                    } else {
                        vidQueue.push(videoData);
                        sendQueueEmbed(videoData)
                    }
                });
            });
        });
    }
}

function sendQueueEmbed(data) {
    let embed = new Discord.RichEmbed();
    embed.setColor("#FF0000");
    embed.setTitle(data.source);
    embed.setDescription(data.queuer + " added a video to the queue");
    embed.setThumbnail(data.thumbnail);
    embed.addField("Video", data.title);
    embed.addField("Uploader", data.uploader, true);
    embed.addField("Length", data.length, true);
    embed.setURL(data.url);
    data.responseChannel.send(embed);
}

function sendNowPlayingEmbed(data) {
    let embed = new Discord.RichEmbed();
    embed.setColor("#FF0000");
    embed.setTitle(data.source);
    embed.setDescription("Playing video requested by " + data.queuer);
    embed.setThumbnail(data.queuerAvatar);
    embed.addField("Video", data.title);
    embed.addField("Uploader", data.uploader, true);
    embed.addField("Length", data.length, true);
    embed.setImage(data.thumbnail);
    embed.setURL(data.url);
    data.responseChannel.send(embed);
}

function encodeBase64AudioStream(audioFilePath, msg, cb) {
    log(`${chalk.gray(`(${msg.guild.id})`)} Encoding audio to base64...`);
    fs.readFile(audioFilePath, function (err, data) {
        let binaryData = data;
        let base64Data = Buffer.from(binaryData, 'binary').toString('base64');
        cb(base64Data);
    });
}

function decodeBase64AudioStream(base64AudioStream, msg, cb) {
    log(`${chalk.gray(`(${msg.guild.id})`)} Decoding base64 audio stream...`);
    let base64Data = base64AudioStream;
    let binaryData = Buffer.from(base64Data, 'base64');
    const Readable = require('stream').Readable;
    const s = new Readable();
    s._read = () => {};
    s.push(binaryData);
    s.push(null);
    cb(s);
}

function convertToAudio(file, base64 = false, msg, cb) {
    var process = new ffmpeg(file);
    process.then(function (video) {
        video.fnExtractSoundToMP3(`${file}.mp3`, function () {
            fs.unlinkSync(`${file}`);
            if (base64) {
                encodeBase64AudioStream(`${file}.mp3`, msg, function (data) {
                    cb(data);
                });
            }
        });
    });
}

function notifyProcessing(channel, customMessage = "Processing...") {
    channel.send(customMessage).then(msg => loadingmsg.push(msg));
}

function stopNotifyProcessing() {
    //loadingmsg.shift().delete();
}

function reloadConfig(server = null) {
    if (server === null) {
        config = require('../config/config.json');
    } else {

    }
}

function reloadTwitch(serverid) {
    log(`Reloading Twitch config for ${chalk.green(serverid)} and restarting Twitch bot.`, 'info', 'twitch');
    let twitchBot = twitchbots[serverid];
    let svrTwitchCfg = require(`../data/${serverid}/twitch.json`);
    if (svrTwitchCfg.channels.length > 0 && svrTwitchCfg.clipchannel !== null) {
        if (twitchBot !== undefined) twitchBot.close();
        else log(`Created Twitch bot for server id ${serverid}`);
        twitchbots[serverid] = new TwitchBot({
            username: apis.twitch.username,
            oauth: apis.twitch.oauth,
            channels: svrTwitchCfg.channels
        });
        setTwitchListeners(serverid);
    } else {
        log(`Invalid Twitch configuration for server ${chalk.green(serverid)}. Some configuration info may be missing. Skipping bot creation for this server.`, 'warn', 'twitch');
    }
}

function setTwitchListeners(serverid) {
    let serverconfig = require(`../data/${serverid}/twitch.json`);
    twitchbots[serverid].on('join', channel => {
        log(`Joined channel: ${channel}`, 'info', 'twitch');
    });
    twitchbots[serverid].on('message', chatter => {
        // log(`${chalk.green(`${chatter.username}`)} sent a message in ${chalk.blue(`${chatter.channel}`)}. Contents: ${chatter.message}`, 'info', 'twitch');
        var pattern = new UrlPattern("(http(s)\\://)clips.twitch.tv(/:id)");
        if (pattern.match(chatter.message)) {
            serverconfig.clipchannel.send(chatter.message);
        }
    });
}

function resetSelf() {
    if (config.devMode) {
        bot.user.setActivity("Development Mode");
    } else {
        bot.user.setActivity(statuses[Math.floor((Math.random() * statuses.length))]);
    }
}

module.exports = Bot;